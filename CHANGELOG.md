# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.6.0](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.5.0...v1.6.0) (2020-10-02)


### Features

* added cookies module ([edd8af4](https://bitbucket.org/alexandrereisloba/commit-convention/commit/edd8af428dc0d0799fe503b687e264c0156a652e))


### Bug Fixes

* removed unwanted console.log ([49cb825](https://bitbucket.org/alexandrereisloba/commit-convention/commit/49cb82562efe3b713a1992084a1254d6d894efc0))

## [1.5.0](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.31...v1.5.0) (2020-10-01)


### Features

* default pipeline workflow for other commits pushes ([7d65977](https://bitbucket.org/alexandrereisloba/commit-convention/commit/7d65977d53f95b19d4d84f13c24b724eaed037b6))
* new automated file module ([54b0f02](https://bitbucket.org/alexandrereisloba/commit-convention/commit/54b0f0269fb203cb0d203e7ecb1c4e2cd4980159))


### Bug Fixes

* documentation commands and pipeline ([a46bd9f](https://bitbucket.org/alexandrereisloba/commit-convention/commit/a46bd9f6636d43a44f9413e6efd96e77666bd83e))
* manual pipeline ([da2a018](https://bitbucket.org/alexandrereisloba/commit-convention/commit/da2a01868b407bf745caf7210b71cda83e4fdc30))

### [1.4.31](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.30...v1.4.31) (2020-10-01)

### [1.4.30](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.29...v1.4.30) (2020-10-01)

### [1.4.29](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.28...v1.4.29) (2020-10-01)

### [1.4.28](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.27...v1.4.28) (2020-10-01)

### [1.4.27](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.26...v1.4.27) (2020-10-01)

### [1.4.26](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.25...v1.4.26) (2020-10-01)

### [1.4.25](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.24...v1.4.25) (2020-10-01)

### [1.4.24](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.23...v1.4.24) (2020-10-01)

### [1.4.23](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.22...v1.4.23) (2020-10-01)

### [1.4.22](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.21...v1.4.22) (2020-10-01)

### [1.4.21](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.20...v1.4.21) (2020-10-01)

### [1.4.20](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.19...v1.4.20) (2020-10-01)

### [1.4.19](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.18...v1.4.19) (2020-10-01)

### [1.4.18](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.17...v1.4.18) (2020-10-01)

### [1.4.17](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.16...v1.4.17) (2020-10-01)

### [1.4.16](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.15...v1.4.16) (2020-10-01)

### [1.4.15](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.14...v1.4.15) (2020-10-01)

### [1.4.14](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.13...v1.4.14) (2020-10-01)

### [1.4.13](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.12...v1.4.13) (2020-10-01)

### [1.4.12](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.11...v1.4.12) (2020-10-01)

### [1.4.11](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.10...v1.4.11) (2020-10-01)

### [1.4.10](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.9...v1.4.10) (2020-10-01)

### [1.4.9](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.8...v1.4.9) (2020-10-01)

### [1.4.8](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.7...v1.4.8) (2020-10-01)

### [1.4.7](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.6...v1.4.7) (2020-10-01)

### [1.4.6](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.5...v1.4.6) (2020-10-01)

### [1.4.5](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.4...v1.4.5) (2020-10-01)

### [1.4.4](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.3...v1.4.4) (2020-10-01)

### [1.4.3](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.2...v1.4.3) (2020-10-01)

### [1.4.2](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.0...v1.4.2) (2020-10-01)


### Bug Fixes

* bitbucket pipeline ([11e8146](https://bitbucket.org/alexandrereisloba/commit-convention/commit/11e8146b65c2680d83861ac72feb927b968c360f))
* bitbucket pipeline with automated commit to branch master ([acf66a4](https://bitbucket.org/alexandrereisloba/commit-convention/commit/acf66a47fdcfb92e16f699dcbbd4bce0986ba031))

### [1.4.1](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.4.0...v1.4.1) (2020-10-01)

## [1.4.0](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.3.1...v1.4.0) (2020-10-01)


### Features

* helloworld module ([7aac1b1](https://bitbucket.org/alexandrereisloba/commit-convention/commit/7aac1b158da3edab0c9cd946d8590a9995717093))

### [1.3.1](https://bitbucket.org/alexandrereisloba/commit-convention/compare/v1.3.0...v1.3.1) (2020-10-01)


### Bug Fixes

* changelog ([9e9c374](https://bitbucket.org/alexandrereisloba/commit-convention/commit/9e9c374bb0d9b46211ed15e5eafe9b36f0d431c3))
