<!-- Cookies -->
<div class="cookies-wrapper">
    <div class="cookies-box">
        <div class="container p-3">
            <div class="cookies__title">Definições de Cookies</div>
            <div class="cookies-description">
                <p class="cookies-bar-message">A <b><?php echo get_bloginfo( 'name' ); ?></b> pode utilizar cookies para
                    memorizar os seus
                    dados de início de sessão, recolher estatísticas para otimizar a funcionalidade do site e para
                    realizar ações de marketing com base nos seus interesses.</p>
                <div>
                    <div class="float-right">
                        <button class="button btn btn-solid" id="accept-cookie"
                            style="background-color: #c82f3c; border: 0; color:#fff">Sim,
                            aceito</button>
                    </div>
                    <div class="cookies-settings-link">
                        <button class="button btn btn-light">Preferências de Cookies</button>
                    </div>

                </div>
            </div>
            <div class="cookies-settings">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="form-group">
                            <input type="radio" name="cookie-radio" id="cookie-required" value="required">
                            <label for="cookie-required" class="label-cookies form-check-label">Cookies
                                Necessários</label>
                            <div class="clearfix"></div>
                            <small class="cookie-info">
                                Estes cookies são necessários para permitir a funcionalidade principal do site e são
                                ativados automaticamente quando
                                utiliza este site.
                            </small>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <input type="radio" name="cookie-radio" id="cookie-functional" value="functional"><label
                                for="cookie-functional" class="label-cookies form-check-label">Cookies
                                Funcionais</label>
                            <div class="clearfix"></div>
                            <small class="cookie-info">
                                Estes cookies permitem-nos analisar a utilização do site, por forma a podermos medir e
                                melhorar o respectivo desempenho.
                            </small>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <input type="radio" name="cookie-radio" id="cookie-complete" value="complete" checked><label
                                for="cookie-complete" class="label-cookies form-check-label">Cookies
                                Publicitários</label>
                            <div class="clearfix"></div>
                            <small class="cookie-info">
                                Permitem-lhe estar em contacto com a sua rede social, partilhar conteúdos, enviar e
                                divulgar comentários.
                            </small>
                        </div>
                    </div>
                    <div class="col-sm-5 cookies-message">
                        <p data-id="cookie-required" style="display: block;">
                            <b>Cookies Necessários</b>
                            Permitem personalizar as ofertas comerciais que lhe são apresentadas, direcionando-as para
                            os seus interesses. Podem ser
                            cookies próprios ou de terceiros. Alertamos que, mesmo não aceitando estes cookies, irá
                            receber ofertas comerciais, mas
                            sem corresponderem às suas preferências.
                        </p>
                        <p data-id="cookie-functional">
                            <b>Cookies Funcionais</b>
                            Oferecem uma experiência mais personalizada e completa, permitem guardar preferências,
                            mostrar-lhe conteúdos relevantes
                            para o seu gosto e enviar-lhe os alertas que tenha solicitado.
                        </p>
                        <p data-id="cookie-complete">
                            <b>Cookies Publicitários</b>
                            Permitem-lhe estar em contacto com a sua rede social, partilhar conteúdos, enviar e divulgar
                            comentários.
                        </p>
                    </div>
                </div>
                <div>
                    <div class="cookies-settings-link">
                        <button class="button btn btn-solid" id="submit-preferences">Submeter Preferências</button>
                        <button class="button btn btn-light">Cancelar</button>
                    </div>
                </div>
            </div>
            <div class="cookies-policy-link">
                <?php $politica = get_permalink( get_page_by_path( 'politica-de-privacidade' ) ); ?>
                <a href="<?php echo($politica); ?>" rel="nofollow" class="cookies-bar-know-more"
                    title="Política de Privacidade" target="_blank">Política de Privacidade</a>
            </div>
        </div>
    </div>
</div>
<!-- Cookies ends -->