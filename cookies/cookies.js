// jQuery document.ready equivalent - faster
document.addEventListener('DOMContentLoaded', function (event) {

  // ES6 example syntax
  // will be transpiled to common javascript through Babel
  var some = () => {
    console.log('Developed by LOBA.cx - https://www.loba.pt/');
  }
  some();


  /***********************************
   *
   * Cookies functions
   *
  **/

  function createCookie(name, value, days) {

    // Condition to check if already exists a
    // different cookie and erase it to create a new one
    (readCookie('eucookie-required') && name != 'eucookie-required') ? eraseCookie('eucookie-required') : '';
    (readCookie('eucookie-functional') && name != 'eucookie-functional') ? eraseCookie('eucookie-functional') : '';
    (readCookie('eucookie-complete') && name != 'eucookie-complete') ? eraseCookie('eucookie-complete') : '';

    if (days) {
      var date = new Date();
      date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
      var expires = '; expires=' + date.toGMTString();
    } else {
      var expires = '';
    }
    document.cookie = name + '=' + value + expires + '; path=/';
  }

  function readCookie(name) {
    var nameEQ = name + '=';
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) == ' ') {
        c = c.substring(1, c.length);
      }
      if (c.indexOf(nameEQ) == 0) {
        // return c.substring(nameEQ.length, c.length);
        return true;
      }
    }
    return null;
  }

  function eraseCookie(name) {
    createCookie(name, '', -1);
  }
  // Cookies functions END

  var cookiesWrapper = document.querySelector('.cookies-wrapper'),
    changeCookies = document.querySelector('.change-cookies'),
    bodyElem = document.querySelector('body'),
    cookiesRadios = document.querySelectorAll('input[name="cookie-radio"]');

  // Show Cookies if not accepted already
  if (
    !readCookie('eucookie-required') &&
    !readCookie('eucookie-functional') &&
    !readCookie('eucookie-complete')
  ) {
    cookiesWrapper.style.display = 'block';
    bodyElem.classList.add('in-modal');
  } else {
    readCookiePermissions();
  }

  if (changeCookies) {
    changeCookies.addEventListener('click', function (e) {
      e.preventDefault();
      cookiesWrapper.style.display = 'block';
    });
  }

  // Click to accept and close cookies
  jQuery('#accept-cookie, #submit-preferences').on('click', function (e) {
    e.preventDefault();
    var cookieVal = 'eucookie-' + jQuery('input[name="cookie-radio"]:checked').val();
    createCookie(cookieVal, cookieVal, 365 * 10);
    readCookiePermissions();
    cookiesWrapper.style.display = 'none';
    bodyElem.classList.remove('in-modal');
  });

  // Listen for changes on Cookies radios
  for (var i = 0; i < cookiesRadios.length; i++) {
    var cookieRadio = cookiesRadios[i];

    cookieRadio.addEventListener('change', function () {
      var cookieID = this.id;

      // get all messages to dismiss them
      var cookiesMessages = document.querySelectorAll('.cookies-message p');
      for (var j = 0; j < cookiesMessages.length; j++) {
        cookiesMessages[j].style.display = 'none';
      }

      // show the message of the selected cookie
      document.querySelector('.cookies-message p[data-id="' + cookieID + '"]').style.display = 'block';
    });
  }

  // open and close cookies preferences
  jQuery('.cookies-settings-link button, .cookies-settings-cancel button').on('click', function (e) {
    jQuery('.cookies-description, .cookies-settings').toggle();
  });



  function readCookiePermissions() {
    // JS code if user accepted only minimum required cookies

    if (readCookie('eucookie-required') || readCookie('eucookie-functional') || readCookie('eucookie-complete')) {


      // JS code if user accepted only functional cookies

      if (readCookie('eucookie-functional') || readCookie('eucookie-complete')) {


        // JS code if user accepted all cookies

        if (readCookie('eucookie-complete')) {

          if (typeof analyticsCode !== 'undefined' && analyticsCode !== null) {
            // Load Google Analytics Code
            (function (b, o, i, l, e, r) {
              b.GoogleAnalyticsObject = l;
              b[l] || (b[l] =
                function () {
                  (b[l].q = b[l].q || []).push(arguments)
                });
              b[l].l = +new Date;
              e = o.createElement(i);
              r = o.getElementsByTagName(i)[0];
              e.src = 'https://www.google-analytics.com/analytics.js';
              r.parentNode.insertBefore(e, r)
            }(window, document, 'script', 'ga'));
            ga('create', analyticsCode, 'auto', {
              anonymizeIp: true
            });
            ga('send', 'pageview');
            // End Google Analytics Code
          }

          if (typeof fbPixelCode !== 'undefined' && fbPixelCode !== null) {
            // Facebook Pixel Code
            !function (f, b, e, v, n, t, s) {
              if (f.fbq) return; n = f.fbq = function () {
                n.callMethod ?
                  n.callMethod.apply(n, arguments) : n.queue.push(arguments)
              };
              if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
              n.queue = []; t = b.createElement(e); t.async = !0;
              t.src = v; s = b.getElementsByTagName(e)[0];
              s.parentNode.insertBefore(t, s)
            }(window, document, 'script',
              'https://connect.facebook.net/en_US/fbevents.js');
            fbq('init', fbPixelCode);
            fbq('track', 'PageView');
            // End Facebook Pixel Code
          }

          if (typeof tagManagerCode !== 'undefined' && tagManagerCode !== null) {
            (function (w, d, s, l, i) {
              w[l] = w[l] || []; w[l].push({
                'gtm.start':
                  new Date().getTime(), event: 'gtm.js'
              }); var f = d.getElementsByTagName(s)[0],
                j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =
                  'https://www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);
            })(window, document, 'script', 'dataLayer', tagManagerCode);
          }

        }
        // cookie-complete code


      }
      // cookie-functional code
    }
  }
  // read cookie permission code

});